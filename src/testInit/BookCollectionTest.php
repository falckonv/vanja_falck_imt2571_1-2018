<?php

require_once('./IMT2571/Assignment1/Model/DBModel.php');
require_once('./IMT2571/Assignment1/Model/dbCredentials.php');

class BookCollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $dbModel;

    // Recreates database for each run - establishing fresh database:
    protected function _before()
    {
        $db = new PDO(
                  'mysql:host='.DB_HOST.';dbname='.DB_NAME.
                  ';charset='.CHARSET.'',DB_USER,DB_PWD,
          // Hardcoded:
          //      'mysql:host=localhost;dbname=test;charset=utf8',
          //      'root',
          //      '',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $this->dbModel = new DBModel($db);
    }

    protected function _after()
    {
    }

    // Test that all books are retrieved from the database
    public function testGetBookList()
    {
        $bookList = $this->dbModel->getBookList();

        // Sample tests of book list contents
        $this->assertEquals(count($bookList), 3);
        $this->assertEquals($bookList[0]->id, 1);
        $this->assertEquals($bookList[0]->title, 'Jungle Book');
        $this->assertEquals($bookList[1]->id, 2);
        $this->assertEquals($bookList[1]->author, 'J. Walker');
        $this->assertEquals($bookList[2]->id, 3);
        $this->assertEquals($bookList[2]->description, 'Written by some smart gal.');
    }

    // Tests that information about a single book is retrieved from the database
    public function testGetBook()
    {
        $book = $this->dbModel->getBookById(1);

        // Sample tests of book list contents
        $this->assertEquals($book->id, 1);
        $this->assertEquals($book->title, 'Jungle Book');
        $this->assertEquals($book->author, 'R. Kipling');
        $this->assertEquals($book->description, 'A classic book.');
    }

    // Tests that get book operation fails if id is not numeric
    public function testGetBookRejected()
    {
        $this->tester->expectException(InvalidArgumentException::class, function() {
            $this->dbModel->getBookById("1'; drop table book;--");
        });
    }

    // Tests that a book can be successfully added and that the id was assigned. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testAddBook()
    {
        $testValues = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => 'Some description'];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->addBook($book);
        $this->dbModel->verifyBook($book, 4);
        // Id was successfully assigned
        $this->assertEquals($book->id, 4);

        $this->tester->seeNumRecords(4, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 4,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
    }

    // Tests that adding a book fails if id is not numeric
    public function testAddBookRejectedOnInvalidId()
    {
      // CODE ADDED ---------------------------------------------
      $testValues = ['title' => 'New book',
                     'author' => 'Some author',
                     'description' => 'Some description',
                     'id' => 'xyz'];
      $book = new Book($testValues['title'],       $testValues['author'],
                       $testValues['description'], $testValues['id']);
      $this->dbModel->addBook($book);
      $this->dbModel->verifyBook($book, $testValues['id']);
      $this->tester->dontSeeInDatabase('book', ['id'     => $testValues['id'],
                                                'title'  => $testValues['title'],
                                                'author' => $testValues['author']]);
      }


    // Tests that adding a book fails mandatory fields are left blank
    public function testAddBookRejectedOnMandatoryFieldsMissing()
    {
      // CODE ADDED ---------------------------------------------
      $testValues = ['title' => '',
                     'author' => '',
                     'description' => 'Some description'];
      $book = new Book ($testValues['title'], $testValues['author'],
                        $testValues['description']);
      //->dbModel->verifyBook($book, 4);
      //$this->dbModel->addBook($book);
      $this->tester->dontSeeInDatabase('book', ['id'      => 4]);
      $this->tester->dontSeeInDatabase('book', ['title'   => $testValues['title']]);
      $this->tester->dontSeeInDatabase('book', ['author'  => $testValues['author']]);

    }

    // Tests that a book record can be successfully modified. Three cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testModifyBook()
    {
      $book = $this->dbModel->getBookById(1);
      $testValues = ['title'        => 'New book',
                     'author'       => 'Some author',
                     'description'  => 'Some description'];
      $book->title = $testValues['title'];
      $book->author = $testValues['author'];
      $book->description = $testValues['description'];

      // Sample tests of book list contents
      $this->assertEquals($book->title, $testValues['title']);
      $this->assertEquals($book->author, $testValues['author']);
      $this->assertEquals($book->description, $testValues['description']);
    }

    // Tests that modifying a book record fails if id is not numeric
    public function testModifyBookRejectedOnInvalidId()
    {
      $book = $this->dbModel->getBookById(2);
      $testValues = ['title'        => 'New book',
                     'author'       => 'Some author',
                     'description'  => 'Some description',
                     'id' => 'x'];
      $book->title = $testValues['title'];
      $book->author = $testValues['author'];
      $book->description = $testValues['description'];
      $book->id = $testValues['id'];
      $this->tester->dontSeeInDatabase('book', ['title'  => $testValues['title']]);
      $this->tester->dontSeeInDatabase('book', ['author' => $testValues['author']]);
      $this->tester->dontSeeInDatabase('book', ['id'     => $testValues['id']]);

    }

    // Tests that modifying a book record fails if mandatory fields are left blank
    public function testModifyBookRejectedOnMandatoryFieldsMissing()
    {
      //CODE ADDED ----------------------------------------------
      $this->dbModel->getBookById(2);
      $testValues = ['title' => '',
                     'author' => '',
                     'description' => 'Some description'];
      $book = new Book ($testValues['title'], $testValues['author'],
                        $testValues['description']);
      $this->dbModel->getBookById(2);
      $this->tester->dontSeeInDatabase('book', ['title'  => $testValues['title']]);
      $this->tester->dontSeeInDatabase('book', ['author' => $testValues['author']]);
      $this->tester->seeInDatabase('book', ['author' => 'J. Walker']);
    }

    // Tests that a book record can be successfully modified.
    public function testDeleteBook()
    {
      // CODE ADDED ---------------------------------------------
      $this->dbModel->deleteBook(1);
      $this->tester->seeNumRecords(2, 'book');
      $this->tester->dontSeeInDatabase('book', ['id'  => 1]);
    }

    // Tests that adding a book fails if id is not numeric
    public function testDeleteBookRejectedOnInvalidId()
    {
      $testValues = ['title'        => 'New book',
                     'author'       => 'Some author',
                     'description'  => 'Some description',
                     'id' => 'x1'];
      $book = new Book ($testValues['title'], $testValues['author'],
                        $testValues['description'], $testValues['id']);
      $this->dbModel->addBook($book);
      $this->dbModel->deleteBook('x1');
      $this->tester->dontSeeInDatabase('book', ['id' => 'x1']);
    }
}
