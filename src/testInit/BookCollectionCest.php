<?php
use Codeception\Util\Locator;

class BookCollectionCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // Test to verify that the booklist is displayed as expected
    public function showBookListTest(AcceptanceTester $I)
    {
        $I->amOnPage('index.php');

        // Book list content
        $I->seeInTitle('Book Collection');
        $I->seeNumberOfElements('table#bookList>tbody>tr', 3);
        // Check sample book values
        $I->see('Jungle Book', 'tr#book1>td:nth-child(2)');
        $I->see('J. Walker', 'tr#book2>td:nth-child(3)');
        $I->see('Written by some smart gal.', 'tr#book3>td:nth-child(4)');
        $I->seeElement('tr#book1>td:first-child>a', ['href' => 'index.php?id=1']);
        $I->seeElement('tr#book2>td:first-child>a', ['href' => 'index.php?id=2']);
        $I->seeElement('tr#book3>td:first-child>a', ['href' => 'index.php?id=3']);

        // Add new book form content
        $I->seeElement('form#addForm>input', ['name' => 'title']);
        $I->seeElement('form#addForm>input', ['name' => 'author']);
        $I->seeElement('form#addForm>input', ['name' => 'description']);
        $I->seeElement('form#addForm>input', ['type' => 'submit',
                                              'value' => 'Add new book']);
    }

    // Test to verify that the book details page is displayed as expected
    public function showBookDetailsTest(AcceptanceTester $I)
    {
        $I->amOnPage('index.php');
        $I->click(1);
        $this->verifyBookDetails($I, 'Jungle Book', 'R. Kipling', 'A classic book.');
        $I->seeLink('Back to book list','index');

        // Buttons for updating and deleting book information
        $I->seeElement('form#modForm>input', ['type' => 'submit',
                                              'value' => 'Update book record']);
        $I->seeElement('form#delForm>input', ['type' => 'submit',
                                              'value' => 'Delete book record']);
    }

    // Test to verify that non-numeric book id's are rejected when requesting book information
    public function invalidBookIdRejectedTest(AcceptanceTester $I)
    {
        $I->amOnPage("index.php?id=1'; drop table book;--");
        $I->seeInTitle('Error Page');
    }

    // Helper function that verifies that the book information on the current page matches the parameter values
    protected function verifyBookDetails(AcceptanceTester $I, String $title, String $author, String $description)
    {
        $I->seeInTitle('Book Details');
        $I->seeElement('form#modForm>input', ['name' => 'title',
                                              'value' => $title]);
        $I->seeElement('form#modForm>input', ['name' => 'author',
                                              'value' => $author]);
        $I->seeElement('form#modForm>input', ['name' => 'description',
                                              'value' => $description]);
    }

    // Test to verify that new books can be added. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"A Girl's memoirs", author=>"Jean d'Arc", description=>"Single quotes (') should not break anything"
    //   4. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function successfulAddBookTest(AcceptanceTester $I)
    {
        $testValues = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => 'Some description'];
        $I->amOnPage('index.php');
        $I->submitForm('#addForm', ['title' => $testValues['title'],
                                    'author' => $testValues['author'],
                                    'description' => $testValues['description']]);

        // Getting booklist with new book added as ID:4
        $I->seeInTitle('Book Collection');
        $I->seeNumberOfElements('table#bookList>tbody>tr', 4);
        $I->see('ID: 4');
        $I->seeElement('tr#book4>td:first-child>a', ['href' => 'index.php?id=4']);
        $I->see($testValues['title'], 'tr#book4>td:nth-child(2)');
        $I->see($testValues['author'], 'tr#book4>td:nth-child(3)');
        $I->see($testValues['description'], 'tr#book4>td:nth-child(4)');
        $I->seeLink('4','index.php?id=4');
   }

    // CODE ADDED -----------------------------------------------
    // Test to verify that adding a book fails if mandatory fields are missing
    public function addBookWithoutMandatoryFieldsTest(AcceptanceTester $I)
    {
        // Testing missing Author with title and description
        $testValues = ['title' => 'Some title',
                       'author' => '',
                       'description' => 'Some description'];
        $I->amOnPage('index.php');
        $I->submitForm('#addForm', ['title' => $testValues['title'],
                       'author' => $testValues['author'],
                       'description' => $testValues['description']]);
        $I->seeInTitle('Error Page');
        // Testing missing Title with author and description
        $testValues = ['title' => '',
                       'author' => 'Some author',
                       'description' => 'Some description'];
        $I->amOnPage('index.php');
        $I->submitForm('#addForm', ['title' => $testValues['title'],
                       'author' => $testValues['author'],
                       'description' => $testValues['description']]);
        $I->seeInTitle('Error Page');
        // Testing missing Author and Title and description
         $testValues = ['title' => '',
                        'author' => '',
                        'description' => ''];
         $I->amOnPage('index.php');
         $I->submitForm('#addForm', ['title' => $testValues['title'],
                        'author' => $testValues['author'],
                        'description' => $testValues['description']]);
         $I->seeInTitle('Error Page');
    }

    // Test to verify that book records can be modified successfully. Four cases should be verified:
    //   1. title=>"Different title", author=>"Different Author", description=>"Different description"
    //   2. title=>"Different title", author=>"Different Author", description=>""
    //   3. title=>"A Girl's memoirs", author=>"Jean d'Arc", description=>"Single quotes (') should not break anything"
    //   4. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function successfulModifyBookTest(AcceptanceTester $I)
    {
        // Setting testvalues for "change"
        $testValues = ['title' => 'Some other title',
                       'author' => 'Some other author',
                       'description' => 'Some other description'];

        // Test changes on id #3 by submitting a form with new values=testValues
        $I->amOnPage('index.php?id=3');
        $I->submitForm('#modForm', [
                       'title' => 'Some other title',
                       'author' => 'Some other author',
                       'description' => 'Some other description',
                                   ], 'submitUpdate book record');

        // Testing on field title if modified
        $I->amOnPage('index.php?id=3');
        $I->seeInField(['name' => 'title'], $testValues['title']);

        // Testing on filed author if modified
        $I->amOnPage('index.php?id=3');
        $I->seeInField(['name' => 'author'], $testValues['author']);

        // Testing on field description if modified
        $I->amOnPage('index.php?id=3');
        $I->seeInField(['name' => 'description'], $testValues['description']);

    }

    // Test to verify that modifying a book fails if mandatory fields are missing
    public function modifyBookWithoutMandatoryFieldsTest(AcceptanceTester $I)
    {
        // Part 1: Setting test values for modifiation without title
        $testValues = ['title' => '',
                       'author' => 'Some other author',
                       'description' => 'Some other description'];
        // Changing id#2 by modForm - without title
        $I->amOnPage('index.php?id=2');
        $I->submitForm('#modForm', [
                       'title' => $testValues['title'],
                       'author' => $testValues['author'],
                       'description' => $testValues['description'],
                                    ], 'submitUpdate book record');
        // Checks if error page is shown after empty title
        $I->seeInTitle('Error Page');

        // Part 2: Setting test values for modification without author
        $testValues = ['title' => 'Some title',
                       'author' => '',
                       'description' => 'Some other description'];
        // Changing id#3 by modForm without author
        $I->amOnPage('index.php?id=3');
        $I->submitForm('#modForm', [
                       'title' => $testValues['title'],
                       'author' => $testValues['author'],
                       'description' => $testValues['description'],
                                    ], 'submitUpdate book record');
        // Checks if error page is shown after empty author
        $I->seeInTitle('Error Page');
    }

    // Test to verify that deleting a book succeeds.
    public function successfulDeleteBookTest(AcceptanceTester $I)
    {
        // Deleting book with id=3
         $I->amOnPage('index.php?id=3');
         // Clicking delete book on id#3
         $I->click('#delForm input[type=submit]');
         // After deleting - check if url is on Book Collection page
         $I->seeInTitle('Book Collection');
         // Check that id3 is not shown
         $I->dontSeeCurrentUrlEquals('index.php?id=3');
    }

    // Test to verify that deleting a book succeeds.
    public function deleteBookWithInvalidIdTest(AcceptanceTester $I)
    {
         $I->amOnPage('index.php?id=x');
         $I->dontSeeCurrentUrlEquals('index.php?id=x');

         $I->amOnPage('index.php?id=99');
         $I->dontSeeCurrentUrlEquals('index.php?id=99');

         $I->amOnPage('index.php?id=-1');
         $I->dontSeeCurrentUrlEquals('index.php?id=-1');
         // Test if ID=1 is deleted
         $I->amOnPage('index.php?id=4');
         $I->dontSeeCurrentUrlEquals('index.php?id=4');
    }
}
