<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing data in a MySQL database using PDO.
 * @author completing: Vanja Falck
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
require_once("dbCredentials.php");
require_once("AbstractModel.php");
require_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @todo implement class functionality.
 */
class DBModel extends AbstractModel
{
    protected $db = null;

    /**
     * @param PDO $db PDO object for the database; a new one will be created if no PDO object
     *                is passed
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
        // Added PDO connection (Vanja Falck) - all database Credentials
        // as strings with .<word>. within the ''
        $this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';
                            charset='.CHARSET.'', DB_USER, DB_PWD,
                            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

// Hardcoding of credentials (not prefered):
//  $this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8',
// 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookList()
    {
        // CODE ADDED ----------------------------------------------
        // Populate the web-interface with data from the database
        // Make book-array to contain the data
        $booklist = array();
        // Get the PDO-connection from the db in DBModel class:
        $stmt = $this->db->query('SELECT title, author, description, id
                                  FROM book ORDER by id');
        // Get the content of the DB (all) only to be uses for small tables!!!
        // Else use while-loop - be aware of outcommenting the end bracket for
        // the while-Loop if this code is used
        //$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // The while-loop code: runs until no rows left to fetch
        // Safer to use this one!
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        // IF fetchAll is used : use the foreach loop
        // Loops through all rows in the external database to collect data and
        // create new books for each row:
        //foreach($rows as $row) {
                $booklist[] = new Book($row['title'], $row['author'],
                                       $row['description'], $row['id']);
        //  }  // <---- This is the end bracket in the foreach loop
      } // <--- This is the end bracket in the while-loop
        // Returns the filled book-array for displaying in html-views:
        return $booklist;
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookById($id)
    {
      // CODE ADDED -------------------------------------------------------
      // Test if id asked for is numeric - sends errormessage if not
      self::verifyId($id);
      // Find the row with the passed $id
      $stmt  = $this->db->prepare('SELECT * FROM book WHERE id = :id');
      // The only value passed to the db
      $stmt->bindValue(':id', $id);
      $stmt->execute();
      // Gets the row as a book object
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      // Input data from db into a new book-object (for viewing)
      $book = new Book( $row['title'], $row['author'], $row['description'],
                        $row['id']);
      // Returns a book from the database:
      return $book;
    }

    /** Adds a new book to the collection.
     * @param Book $book The book to be added - the id of the book will be set after successful insertion.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function addBook($book)
    {
    // CODE ADDED --------------------------------------------------------
    // Prepare for INSERT in db of new book-object with user-inserted data
    $stmt = $this->db->prepare('INSERT INTO book (title, author, description)
                                VALUES(:title, :author, :description)');
    // getting ERROR - array to string conversion for code below
    $stmt->bindValue(':title',       $book->title);
    $stmt->bindValue(':author',      $book->author);
    $stmt->bindValue(':description', $book->description);
    $stmt->execute();
    // Checks if mandatory fields are in place given a valid id (not yet
    // retreived)

// FEIL I DENNE KODEN --- get errormessage for empty mandatory fields, but
// the web version still inserts empty rows! It gives an id.

    // Get the latest new id from database
    //$a = $this->db->lastInsertId();
    $id = $this->db->lastInsertId();
    // Checks if the returned id from the database is a valid integer
    self::verifyId($id);
    // Checks if al mandatory fields is in place
    self::verifyBook($book, true);
    // Assigns the id-number to the book-object (for viewing)
    $book->id = $id;
    }

    /** Modifies data related to a book in the collection.
     * @param Book $book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function modifyBook($book)
    {
      self::verifyBook($book, $book->id);
      $stmt = $this->db->prepare('UPDATE book SET title = :title,
          author = :author, description = :description WHERE id = :id');
      // getting ERROR - array to string conversion for code below
      $stmt->bindValue(':title',        $book->title);
      $stmt->bindValue(':author',       $book->author);
      $stmt->bindValue(':description',  $book->description);
      $stmt->bindValue(':id',           $book->id);
      $stmt->execute();
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function deleteBook($id)
    {
      // CODE ADDED ------------------------------------------------
      $stmt  = $this->db->prepare('DELETE FROM book WHERE id = :id');
      $stmt->bindValue(':id', $id);
      $stmt->execute();
    }
}
