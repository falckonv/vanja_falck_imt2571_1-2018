IMT2571_2018 Assessment 1

Content:
Report
Code changes

Note:

Last update September 27th 2018

Finished both UML diagrams
Coded DBModel.php
Coded BookCollectionTest.php
Coded BookCollectionCest.php

Acceptance test succeded for session model (Model.php)
Unit test succeded for dbModel (dbModel.php)
Acceptance test succeded for dbModel
